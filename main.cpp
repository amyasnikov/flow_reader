
#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>
#include <vector>
#include <stack>

#define PR std::cout << "#" << __LINE__ << " : " << __PRETTY_FUNCTION__ << std::endl
#define PRM(x) std::cout << "#" << __LINE__ << " : " << __PRETTY_FUNCTION__ << "\t" << (x) << std::endl

std::string hex(const void* ptr, size_t len) {
  std::stringstream sstream;
  sstream << "{ " << std::hex;
  for (size_t i{}; i < len; ++i) {
    sstream << "0x" << (int) ((const uint8_t*) ptr)[i] << " ";
  }
  sstream << "} " << std::dec << len << "s";
  return sstream.str();
}

struct flow_empty_t { };

struct device_t {
  virtual size_t read(void *data, size_t len) = 0;
  virtual bool is_close() = 0;
};

struct device_file_t : device_t {
  std::ifstream file;

  device_file_t(const std::string name) : file(name) { }

  size_t read(void *data, size_t len) override {
    return file.readsome(static_cast<char*>(data), len);
  }

  bool is_close() override {
    return file.rdbuf()->in_avail() <= 0;
  }
};

struct buffer_t {
  std::deque<uint8_t> data;

  size_t skip(size_t len) {
    PR;
    if (data.size() < len)
      throw flow_empty_t();
    data.erase(data.begin(), data.begin() + len);
    return len;
  }

  size_t read(void *buf, size_t len) {
    PR;
    if (data.size() < len)
      throw flow_empty_t();
    std::copy(data.begin(), data.begin() + len, static_cast<uint8_t*>(buf));
    data.erase(data.begin(), data.begin() + len);
    return len;
  }
};

struct pcap_hdr_t {
  uint32_t magic_number;   /* magic number */
  uint16_t version_major;  /* major version number */
  uint16_t version_minor;  /* minor version number */
  int32_t  thiszone;       /* GMT to local correction */
  uint32_t sigfigs;        /* accuracy of timestamps */
  uint32_t snaplen;        /* max length of captured packets, in octets */
  uint32_t network;        /* data link type */
};

struct pcaprec_hdr_t {
  uint32_t ts_sec;         /* timestamp seconds */
  uint32_t ts_usec;        /* timestamp microseconds */
  uint32_t incl_len;       /* number of octets of packet saved in file */
  uint32_t orig_len;       /* actual length of packet */
};



struct decoder_t;

struct reader_data_t {
  size_t     ind = 0;
  decoder_t* decoder = nullptr;

  virtual void read() = 0;
};

template <typename T>
struct reader_pod_t : reader_data_t {
  T& value;

  reader_pod_t(T& value) : value(value) { }

  void read() override;
};

using reader_uint16_t = reader_pod_t<uint16_t>;
using reader_uint32_t = reader_pod_t<uint32_t>;
using reader_int32_t  = reader_pod_t<int32_t>;

struct reader_pcap_t : reader_data_t {

  void read() override;
};

struct reader_pcap_hdr_t : reader_data_t {
  pcap_hdr_t pcap_hdr;

  void read() override;
};

struct reader_pcaprec_hdr_t : reader_data_t {
  pcaprec_hdr_t pcaprec_hdr;

  void read() override;
};

struct stack_states_t {
  std::stack<std::shared_ptr<reader_data_t>> states;
};

struct decoder_t {
  stack_states_t stack_states;
  std::shared_ptr<device_t> device;
  buffer_t                  buffer;

  decoder_t() {
    device = std::make_shared<device_file_t>("teradata_odbc.pcap");
  }

  void decode() {
    PR;

    while (true) {

      { // move data from devite to buffer
        uint8_t data[256];
        size_t len = device->read(data, sizeof(data));
        buffer.data.insert(buffer.data.end(), data, data + len);
        PRM("buffer.size(): " + std::to_string(buffer.data.size()));
      }

      try {
        while (true) { // decode data from buffer
          if (stack_states.states.empty()) {
            auto reader = std::make_shared<reader_pcap_t>();
            reader->decoder = this;
            stack_states.states.push({reader});
          }

          stack_states.states.top()->read();
        }
      } catch (flow_empty_t) {
        PRM("flow_empty_t");
      }

      if (device->is_close()) {
        break;
      }

    }
  }
};

template <typename T>
void reader_pod_t<T>::read() {
  PR;
  switch (ind) {
    case 0: {
      decoder->buffer.read(&value, sizeof(value));
      ind++;
    }
    default: {
      decoder->stack_states.states.pop();
    }
  }
}

void reader_pcap_t::read() {
  PR;
  switch (ind) {
    case 0: {
      auto reader = std::make_shared<reader_pcap_hdr_t>();
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 1: {
      auto reader = std::make_shared<reader_pcaprec_hdr_t>();
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      return;
    }
    default: {
    }
  }
}

void reader_pcap_hdr_t::read() {
  PR;
  switch (ind) {
    case 0: {
      auto reader = std::make_shared<reader_uint32_t>(pcap_hdr.magic_number);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 1: {
      auto reader = std::make_shared<reader_uint16_t>(pcap_hdr.version_major);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 2: {
      auto reader = std::make_shared<reader_uint16_t>(pcap_hdr.version_minor);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 3: {
      auto reader = std::make_shared<reader_int32_t>(pcap_hdr.thiszone);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 4: {
      auto reader = std::make_shared<reader_uint32_t>(pcap_hdr.sigfigs);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 5: {
      auto reader = std::make_shared<reader_uint32_t>(pcap_hdr.snaplen);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 6: {
      auto reader = std::make_shared<reader_uint32_t>(pcap_hdr.network);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    default: {
      PRM("magic_number:  " + hex(&pcap_hdr.magic_number,  sizeof(pcap_hdr.magic_number)));
      PRM("version_major: " + hex(&pcap_hdr.version_major, sizeof(pcap_hdr.version_major)));
      PRM("version_minor: " + hex(&pcap_hdr.version_minor, sizeof(pcap_hdr.version_minor)));
      PRM("thiszone:      " + hex(&pcap_hdr.thiszone,      sizeof(pcap_hdr.thiszone)));
      PRM("sigfigs:       " + hex(&pcap_hdr.sigfigs,       sizeof(pcap_hdr.sigfigs)));
      PRM("snaplen:       " + hex(&pcap_hdr.snaplen,       sizeof(pcap_hdr.snaplen)));
      PRM("network:       " + hex(&pcap_hdr.network,       sizeof(pcap_hdr.network)));
      decoder->stack_states.states.pop();
    }
  }
}

void reader_pcaprec_hdr_t::read() {
  PR;
  switch (ind) {
    case 0: {
      auto reader = std::make_shared<reader_uint32_t>(pcaprec_hdr.ts_sec);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 1: {
      auto reader = std::make_shared<reader_uint32_t>(pcaprec_hdr.ts_usec);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 2: {
      auto reader = std::make_shared<reader_uint32_t>(pcaprec_hdr.incl_len);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      return;
    }
    case 3: {
      auto reader = std::make_shared<reader_uint32_t>(pcaprec_hdr.orig_len);
      reader->decoder = decoder;
      decoder->stack_states.states.push({reader});
      ind++;
      PRM("ts_sec:   " + hex(&pcaprec_hdr.ts_sec,   sizeof(pcaprec_hdr.ts_sec)));
      PRM("ts_usec:  " + hex(&pcaprec_hdr.ts_usec,  sizeof(pcaprec_hdr.ts_usec)));
      PRM("incl_len: " + hex(&pcaprec_hdr.incl_len, sizeof(pcaprec_hdr.incl_len)));
      PRM("orig_len: " + hex(&pcaprec_hdr.orig_len, sizeof(pcaprec_hdr.orig_len)));
      return;
    }
    case 4: {
      decoder->buffer.skip(pcaprec_hdr.incl_len); // TODO
      ind++;
      return;
    }
    default: {
      decoder->stack_states.states.pop();
    }
  }
}

int main() {
  PR;
  decoder_t decoder;
  decoder.decode();

  return 0;
}

